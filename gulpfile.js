"use strict";

const { src, dest, watch, parallel, series } = require("gulp");
const scss = require("gulp-sass")(require("sass"));
const browserSync = require("browser-sync").create();
const minifyjs = require("gulp-js-minify");
const uglify = require("gulp-uglify");
const cleanCSS = require("gulp-clean-css");
const clean = require("gulp-clean");
const concat = require("gulp-concat");
const imagemin = require("gulp-imagemin");
const autoprefixer = require("gulp-autoprefixer");

// Paths
const srcPath = "src/";
const distPath = "dist/";

const path = {
  build: {
    html: distPath,
    css: distPath + "css/",
    js: distPath + "js/",
    img: distPath + "img/",
  },
  src: {
    html: srcPath + "*.html",
    css: srcPath + "scss/*.scss",
    js: srcPath + "js/*.js",
    img: srcPath + "img/**/*",
  },
  watch: {
    html: srcPath + "**/*.html",
    css: srcPath + "scss/**/*.scss",
    js: srcPath + "js/**/*.js",
    img: srcPath + "img/**/*",
  },
  clean: "./" + distPath,
};

function html() {
  return src(path.src.html, { base: srcPath })
    .pipe(dest(path.build.html))
    .pipe(browserSync.stream());
}

function styles() {
  return src(path.src.css, { base: srcPath + "scss/" })
    .pipe(scss({ outputStyle: "compressed" }))
    .pipe(concat("styles.min.css"))
    .pipe(autoprefixer({ overrideBrowserslist: ["last 10 version"] }))
    .pipe(cleanCSS())
    .pipe(dest(path.build.css))
    .pipe(browserSync.stream());
}

function scripts() {
  return src(path.src.js, { base: srcPath + "js/" })
    .pipe(concat("scripts.min.js"))
    .pipe(uglify())
    .pipe(minifyjs())
    .pipe(dest(path.build.js))
    .pipe(browserSync.stream());
}

function images() {
  return src(path.src.img, { base: srcPath + "img/" })
    .pipe(imagemin())
    .pipe(dest(path.build.img))
    .pipe(browserSync.stream());
}

function cleanDist() {
  return src(path.clean).pipe(clean());
}

function watching() {
  watch([path.watch.html], html);
  watch([path.watch.css], styles);
  watch([path.watch.js], scripts);
  watch([path.watch.img], images);
}

function browsersync() {
  browserSync.init({
    server: {
      baseDir: "./" + distPath,
    },
  });
}
const build = series(cleanDist, parallel(html, styles, scripts, images));

// Dev task
const dev = series(build, parallel(watching, browsersync));

exports.build = build;
exports.dev = dev;
exports.html = html;
exports.styles = styles;
exports.scripts = scripts;
exports.images = images;
exports.cleanDist = cleanDist;
exports.watching = watching;
exports.browsersync = browsersync;
