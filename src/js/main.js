const burgerBtn = document.querySelector(".burger-btn");
const menu = document.querySelector(".burger-menu");

burgerBtn.addEventListener("click", () => {
  burgerBtn.classList.toggle("clicked");
  menu.classList.toggle("clicked");
});

const selectItem = document.querySelectorAll(".burger-menu__link");
selectItem.forEach((el) => {
  el.addEventListener("click", (e) => {
    e.preventDefault();
    switchElement(e.target);
    switchTextColor();
  });
});
function switchElement(element) {
  selectItem.forEach((item) => {
    item.classList.remove("active");
    if (item === element) {
      item.classList.add("active");
    }
  });
}
function switchTextColor() {
  const activeColor = "#8D81AC";
  const inactiveColor = "#FFFFFF";

  selectItem.forEach((item) => {
    if (item.classList.contains("active")) {
      item.style.color = activeColor;
    } else {
      item.style.color = inactiveColor;
    }
  });
}
